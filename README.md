# InformeCotxes

En aquest programa es pot donar d'alta un cotxe amb les seves característiques i es pot veure un informe amb els diversos automobils que s'hagin documentat.
És necessita tenir tots els arxius del projecte descarregats i al mateix repositori per a que l'informe estigui més detallat i que la informació estigui ordenada en files i columnes.

# Exemple execució

![Image text](Captura de 2022-03-01 18-35-14.png)

Al seleccionar 1:

![Image text](Captura de 2022-03-01 19-28-34.png)

Al seleccionar 2:

![Image text](Captura de 2022-03-01 19-26-51.png)

Al seleccionar 3:

![Image text](Captura de 2022-03-01 18-36-37.png)

Al seleccionar 0:

![Image text](Captura de 2022-03-01 19-28-16.png)
