#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define BB while(getchar()!='\n')
#define NOM_FITXER "cotxes.dat"
#define MAX_MATRICULA 8
#define MAX_MARCA 25
#define MAX_MODEL 30
#define MAX_CAP_HTML 1000
#define MAX_FOT_HTML 200
#define MAX_INFORME 10000
#define MAX 100

typedef struct {
    char matricula[MAX_MATRICULA+1];
    char marca[MAX_MARCA+1];
    char model[MAX_MODEL+1];
    int numPortes;
    int potencia;
    double consum;
    bool teEtiquetaECO;
} Cotxes; //estructura que representa el fitxer intern.

void entrarCotxe(Cotxes *cotxe);
void escriureCotxes(Cotxes cotxe);
void printarMenu();
int alta(char nomFitxer[]);
int llistat(char nomFitxer[]);
int generarInforme(char nomFitxer[]);

/*
Crear una estructura per salvar dades de cotxes

Matricula (cadena),
Marca (cadena),
Model (cadena),
númeroPortes (enter),
Potència (enter),
Consum (double),
TeEtiquetaECO (booleà).

***Menú***
1) Alta
2) Llistat

0) Sortir

*************

1) a) Demana a l'usuari, per teclat, les dades d'un cotxe.
   b) Un cop introduides cal persistir-les al fitxer ("cotxes.dat")
   c) Un cop feta la persistencia de les dades cal demanar a l'usuari si vol introduir les dades d'un altre cotxe. Cas afirmatiu retorna al punt (a). Altrament retorna al menú.

2) Llista per pantalla totes les dades del fitxer i fer una pausa. En prémer qualsevol tecla retorna al menú.

0) Acaba el programa.
*/

int main()
{
    int opcio,error;
    do{
        printarMenu();
        scanf("%d",&opcio);BB;
        switch(opcio){
            case 1:
                error=alta(NOM_FITXER);
                if(error==-1) printf("Error en obrir el fitxer");
                if(error==-2) printf("Error de escriptura al fitxer");
                break;
            case 2:
                error=llistat(NOM_FITXER);
                if(error==-1) printf("Error en obrir fitxer");
                if(error==-3) printf("Error de lectura");
                break;
            case 3:
                error=generarInforme(NOM_FITXER);
                if(error==-1) printf("Error en obrir el fitxer");
                if(error==-3) printf("Error de lectura");
                if(error==-4) printf("Error en obrir fitxer HTML");
                break;
            case 0:
                error=0;
                break;
            default:
                printf("Opcio incorrecta. Les opcions son de 0 a 3\n");
        }
        if(error!=0){
            printf("\nPrem una tecla per continuar...");
            getchar();
        }
    }while(opcio!=0);
}

void altaCotxe(Cotxes *cotxe){
    char etiqueta;
    printf("\nIntrodueix la Matricula: ");
    scanf("%7[^\n]",cotxe->matricula);BB;
    printf("\nIntrodueix la Marca: ");
    scanf("%25[^\n]",cotxe->marca);BB;
    printf("\nIntrodueix el Model: ");
    scanf("%30[^\n]",cotxe->model);BB;
    printf("\nIntrodueix el numero de portes: ");
    scanf("%d",&cotxe->numPortes);BB;
    printf("\nIntrodueix la Potencia: ");
    scanf("%d",&cotxe->potencia);BB;
    printf("\nIntrodueix el Consum: ");
    scanf("%lf",&cotxe->consum);BB;
    do{
        printf("\nTe etiqueta ECO (s/n)? ");
        scanf("%c",&etiqueta);BB;
    }while(etiqueta!='s' && etiqueta!='n');
    if(etiqueta=='s'){
            cotxe->teEtiquetaECO=true;
    }else{
        cotxe->teEtiquetaECO=false;
    }
}

void escriureCotxes(Cotxes cotxe){
    printf("\nLa matricula: %s", cotxe.matricula);
    printf("\nLa marca: %s",cotxe.marca);
    printf("\nEl model: %s",cotxe.model);
    printf("\nEl numero de portes: %d",cotxe.numPortes);
    printf("\nLa potencia: %d",cotxe.potencia);
    printf("\nEl consum: %.2lf",cotxe.consum);
    if(cotxe.teEtiquetaECO==true){
            printf("\nTe etiqueta ECO\n");
    }else{
        printf("\nNo te etiqueta ECO\n");
    }
}

void printarMenu(){
    system("clear || cls");
    printf("\n***MENU***\n");
    printf("\n1-Alta");
    printf("\n2-Llistat");
    printf("\n3-Generar Informe");
    printf("\n\n0-Sortir");
    printf("\nTria Opcio(del 0 al 3): ");
}

int alta(char nomFitxer[]){
    int n;
    Cotxes c1;
    FILE *f1;
    f1=fopen(nomFitxer,"ab");
    if(f1==NULL) return -1;
    altaCotxe(&c1);
    n=fwrite(&c1,sizeof(Cotxes),1,f1);
    if(n==0) return -2;
    fclose(f1);
    return 0;
}

int llistat(char nomFitxer[]){
    int n;
    Cotxes c1;
    FILE *f1;
    f1=fopen(nomFitxer,"rb");
    if(f1==NULL) return -1;
    system("clear || cls");//"clear" per a linux i "cls" per a Windows
    while(!feof(f1)){
        n=fread(&c1,sizeof(Cotxes),1,f1);
        if(!feof(f1)){
            if(n==0) return -3;
            escriureCotxes(c1);
        }
    }
    fclose(f1);
    printf("\nPrem una tecla per continuar...");getchar();
    return 0;
}

int generarInforme(char nomFitxer[]){
    char capHTML[MAX_CAP_HTML+1]="<!DOCTYPE html> \
        <html lang=\"es\"> \
          <head> \
            <meta charset=\"UTF-8\"> \
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
            <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\"> \
            <title>Cotxes</title> \
            <link rel=\"stylesheet\" href=\"reset.css\"> \
            <link rel=\"stylesheet\" href=\"styles.css\"> \
          </head> \
          <body class=\"app\"> \
            <header class=\"header\"> \
                <h1>Informe De Cotxes</h1> \
            </header> \
            <sectio class=\"section\"> \
                <table class=\"table\"> \
                    <tr class=\"table-th\"> \
                        <th>Matricula</th> \
                        <th>Marca</th> \
                        <th>Model</th> \
                        <th>NumPortes</th> \
                        <th>Potencia</th> \
                        <th>Consum</th> \
                        <th>Etiqueta ECO</th> \
                    </tr>";
    char informe[MAX_INFORME];
    informe[0]='\0';
    char footHTML[MAX_FOT_HTML]="</table> \
            </sectio> \
            <footer class=\"footer\"> \
                &copy; Oriol Verdugo Soria<br> \
                M3-UF3 \
            </footer> \
          </body> \
        </html>";
    int n;
    char texttemp[MAX];
    Cotxes c1;
    FILE *f1, *f2;
    f1=fopen("informe.html","w");
    if(f1==NULL) return -4;
    f2=fopen(nomFitxer,"r");
    if(f2==NULL) return -1;

    strcat(informe,capHTML);

    while(!feof(f2)){
        n=fread(&c1,sizeof(Cotxes),1,f2);
        if(!feof(f2)){
            if(n==0) return -3;
            strcat(informe,"<tr><td>");
            sprintf(texttemp,"%s",c1.matricula);
            strcat(informe,texttemp);
            strcat(informe,"</td>");
            sprintf(texttemp,"%s",c1.marca);
            strcat(informe,"<td>");
            strcat(informe,texttemp);
            strcat(informe,"</td>");
            sprintf(texttemp,"%s",c1.model);
            strcat(informe,"<td>");
            strcat(informe, texttemp);
            strcat(informe,"</td>");
            sprintf(texttemp,"%d",c1.numPortes);
            strcat(informe,"<td>");
            strcat(informe,texttemp);
            strcat(informe,"</td>");
            sprintf(texttemp,"%d",c1.potencia);
            strcat(informe,"<td>");
            strcat(informe,texttemp);
            strcat(informe,"</td>");
            sprintf(texttemp,"%.2lf",c1.consum);
            strcat(informe,"<td>");
            strcat(informe,texttemp);
            strcat(informe,"</td>");
            if(c1.teEtiquetaECO) strcpy(texttemp,"Si");
            else strcpy(texttemp,"No");
            strcat(informe,"<td>");
            strcat(informe,texttemp);
            strcat(informe,"</td></tr>");
        }
    }
    strcat(informe,footHTML);

    fputs(informe,f1);

    fclose(f1);
    fclose(f2);

    system("firefox informe.html");
    system("clear || cls");
    printf("\ninforme.html generat correctament");
    printf("\nPrem una tecla per continuar...");getchar();
    return 0;
}
